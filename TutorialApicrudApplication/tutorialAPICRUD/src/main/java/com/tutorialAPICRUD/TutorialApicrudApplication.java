package com.tutorialAPICRUD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialApicrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(TutorialApicrudApplication.class, args);
	}

}
